#!/bin/bash
HERE=$(pwd)
source ../../env.sh
source ../../../../common-scripts/common.sh
from_host

set -x

pushd build
set +x
echo "+ source ${HOST_NFS}/${YOCTO_VERSION}/${YOCTO_ML}/${TARGET_BOARD}-qt/environment-setup-${YOCTO_TOOLCHAIN_SHORT}-${DISTRO}-linux-gnueabi"
source ${HOST_NFS}/${YOCTO_VERSION}/${YOCTO_ML}/${TARGET_BOARD}-qt/environment-setup-${YOCTO_TOOLCHAIN_SHORT}-${DISTRO}-linux-gnueabi

set -x

cmake ../src
make

# no idea why I need this hack here
MAKE="$(which make)"

if [ ! -d ${HOST_NFS}/${YOCTO_VERSION}/${YOCTO_ML}/${TARGET_BOARD}-qt/${TARGET_CONFIGURATION_QT}-${YOCTO_ML}/usr/local/bin ]; then
   sudo mkdir -p ${HOST_NFS}/${YOCTO_VERSION}/${YOCTO_ML}/${TARGET_BOARD}-qt/${TARGET_CONFIGURATION_QT}-${YOCTO_ML}/usr/local/bin
else
   echo "+ ${HOST_NFS}/${YOCTO_VERSION}/${YOCTO_ML}/${TARGET_BOARD}-qt/${TARGET_CONFIGURATION_QT}-${YOCTO_ML}/usr/local/bin exists"
fi
sudo -E ${MAKE} install DESTDIR=${HOST_NFS}/${YOCTO_VERSION}/${YOCTO_ML}/${TARGET_BOARD}-qt/${TARGET_CONFIGURATION_QT}-${YOCTO_ML}

popd

set +x

echo "+ execute /usr/local/bin/hello-qt on your target - assuming the Qt libs are there"

cd ${HERE}
plus_echo "$(basename ${0}) done"


#!/bin/bash
#pushd build
#cmake ../src
#make
#popd

